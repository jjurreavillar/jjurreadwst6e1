-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 16, 2016 at 01:28 PM
-- Server version: 5.5.52-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `encuestas`
--

-- --------------------------------------------------------

--
-- Table structure for table `encuesta`
--

CREATE TABLE IF NOT EXISTS `encuesta` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `textoPregunta` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `encuesta`
--

INSERT INTO `encuesta` (`id`, `textoPregunta`) VALUES
(1, '¿Crees que aprobarás este examen?'),
(2, '¿Quien ganará el mundial de fórmula uno?');

-- --------------------------------------------------------

--
-- Table structure for table `responde`
--

CREATE TABLE IF NOT EXISTS `responde` (
  `usuario` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `idEncuesta` int(10) NOT NULL,
  `cuando` datetime NOT NULL,
  `desde` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`usuario`,`idEncuesta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `respuesta`
--

CREATE TABLE IF NOT EXISTS `respuesta` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `idEncuesta` int(10) NOT NULL,
  `textoRespuesta` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `numeroRespuestas` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `respuesta`
--

INSERT INTO `respuesta` (`id`, `idEncuesta`, `textoRespuesta`, `numeroRespuestas`) VALUES
(1, 1, 'Por supuesto, mínimo un 10', 3),
(5, 1, 'Con un 5 ya me conformaba yo', 13),
(6, 1, 'Puede que si y puede que no', 1),
(7, 1, 'Ni de coña', 0),
(8, 2, 'Kimi Raikkonen', 0),
(9, 2, 'Fernando Alonso', 0),
(10, 2, 'Felipe Massa', 0),
(11, 2, 'Lewis Hamilton', 0);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `login` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `password` binary(40) NOT NULL,
  `tipoUsuario` enum('admin','user') COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
