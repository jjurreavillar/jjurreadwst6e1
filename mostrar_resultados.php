<?php
require("utils.php");
$db = conectarDB("encuestas", "daw", "daw");

$resultado = consultarDB($db, "SELECT textoPregunta FROM encuesta WHERE ".$_POST['encuestaId']." = id");
if (!$resultado)
    echo "<p>Error en la consulta.</p>";
else
    foreach($resultado as $valor)
        $cabecera = $valor['textoPregunta'];

$resultado = consultarDB($db, "UPDATE respuesta SET numeroRespuestas = 0 WHERE numeroRespuestas is NULL");
if (!$resultado)
    echo "<p>Error en la consulta.</p>";

$resultado = consultarDB($db, "UPDATE respuesta SET numeroRespuestas = numeroRespuestas + 1 WHERE ".$_POST['id']." = id");
if (!$resultado)
    echo "<p>Error en la consulta.</p>";

$resultado = consultarDB($db, "SELECT numeroRespuestas,textoRespuesta FROM respuesta WHERE ".$_POST['encuestaId']." = idEncuesta");

if (!$resultado)
    echo "<p>Error en la consulta.</p>";
else
    foreach($resultado as $valor)
    {
        $respuestas[] = $valor['textoRespuesta'];
        $votos[] = $valor['numeroRespuestas'];
    }

$suma = 0;
if (isset($votos))
    foreach($votos as $voto)
        $suma += $voto;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="latin1">
        <style>
            table, th, td
            {
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
        <h1>RESULTADOS ENCUESTA #<?=$_POST['encuestaId']?></h1>
        <table>
        <tr>
            <th><?=$cabecera?></th>
            <th>Total votaciones: <?=$suma?></th>
        </tr>
<?php
foreach ($respuestas as $indice => $respuesta)
{
    echo "<tr>";
    echo "<td>";
    echo $respuesta;
    echo "</td>";
    echo "<td>";
    echo (int)($votos[$indice] * 100 / $suma)." % ($votos[$indice] votos)";
    echo "</td>";
    echo "</tr>";
}
?>
        </table>
<?php

$db = null;
?>
    </body>
</html>

