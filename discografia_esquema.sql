-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 16, 2016 at 01:27 PM
-- Server version: 5.5.52-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Discografia`
--

-- --------------------------------------------------------

--
-- Table structure for table `Album`
--

CREATE TABLE IF NOT EXISTS `Album` (
  `codigo` int(7) NOT NULL,
  `titulo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `discografica` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  `formato` enum('cassette','vinilo','cd','dvd','mp3') COLLATE latin1_spanish_ci NOT NULL,
  `fechaLanzamiento` date DEFAULT NULL,
  `fechaCompra` date DEFAULT NULL,
  `precio` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Cancion`
--

CREATE TABLE IF NOT EXISTS `Cancion` (
  `titulo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `album` int(7) NOT NULL,
  `posicion` int(2) NOT NULL,
  `duracion` time DEFAULT NULL,
  `version` enum('S','N') COLLATE latin1_spanish_ci NOT NULL,
  `genero` enum('Acústica','Banda sonora','Blues','Electrónica','Folk','Jazz','New age','Pop','Rock') COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`titulo`,`album`),
  KEY `album` (`album`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Cancion`
--
ALTER TABLE `Cancion`
  ADD CONSTRAINT `Cancion_ibfk_1` FOREIGN KEY (`album`) REFERENCES `Album` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
