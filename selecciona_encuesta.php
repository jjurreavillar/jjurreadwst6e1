<?php
require("utils.php");
$db = conectarDB("encuestas", "daw", "daw");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="latin1">
    </head>
    <body>
        <h1>SELECCIONE UNA ENCUESTA</h1>
<?php
$resultado = consultarDB($db, "SELECT * FROM encuesta");

if (!$resultado)
    echo "<p>Error en la consulta.</p>";
else
{
    foreach($resultado as $indice => $valor)
    {
        $idx = $indice + 1;
        echo "<a href=\"ver_encuesta.php?encuestaId=$idx\">#$valor[id] - $valor[textoPregunta]</p><br>";
    }
}

$db = null;
?>
    </body>
</html>

