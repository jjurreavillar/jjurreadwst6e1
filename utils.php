<?php
function conectarDB($dbName, $usuario, $pw, $host = "localhost")
{
    try
    {
        $pdo = new PDO("mysql:host=$host;dbname=$dbName", $usuario, $pw);
        $pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);

        return $pdo;
    }
    catch (PDOException $e)
    {
        header("Error grave");
        echo "<p>ERROR: No se ha podido conectar a la base de datos.</p>";
        echo "<br>";
        echo "<p>ERROR: " . $e->getMessage() . "</p>";
        die();
        exit();
    }
}

function consultarDB($db, $query)
{
    return $db->query($query);
}
?>
